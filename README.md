# Wator

WA-TOR ist ein fiktiver toroidaler (schlau für donutförmig) Planet auf dem Fische und Haie um ihr Überleben k&auml;mpfen. \
Während sich die Fische von unsichtbaren und unerschöpflichen Plankton-Quellen ernähren, sind die Haie auf regelmäßige Fischefänge angewiesen.

Eine Welt steht zu führen an. Halte das Gleich­gewicht oder entscheide dich für eine Seite. \
Versuch dein Glück auf: https://daniels-page.web.app/Wator/

<div align="center">
    <img src="images/screenshot_1.png" alt="screenshot_1" height="300"/>
    <img src="images/screenshot_2.png" alt="screenshot_2" height="300"/>
</div>
