var round = 0;

// Das Animal-Objekt
function Animal(type, x, y, color, breed, starve, changecolor) {
	this.type = type;
	this.x = parseInt(x);
	this.y = parseInt(y);
	this.color = color;
	this.breed = parseInt(breed);
	this.starve = parseInt(starve);
	this.energy = parseInt(starve);
	this.changecolor = parseInt(changecolor);
	this.time = 0;
}

// Fügt dem Sea-Feld ein Animal-Objekt hinzu
Animal.prototype.add = function (sea) {
	sea[this.x][this.y] = this;
}

// Entfernt ein Animal-Objekt aus dem Sea-Feld
Animal.prototype.remove = function (sea) {
	delete sea[this.x][this.y];
}

// Zeichnet ein Animal-Objekt auf dem Canvas
Animal.prototype.draw = function (ctx) {
	ctx.fillStyle = this.color;
	ctx.fillRect(this.x, this.y, 1, 1);
}

// Ändert den Hue-Wert des Animal-Farbe
Animal.prototype.changeColor = function () {
	if (this.changecolor != 0) {
		var color = tinycolor(this.color);
		var hsv = color.toHsv();
		hsv.h = ((hsv.h + this.changecolor) % 360 + 360) % 360;;
		color = tinycolor(hsv);
		this.color = color.toHexString();
	}
}

// Erzeugt auf dem dem Feld eines Animal-Objekts einen Nachkommen
Animal.prototype.reproduce = function (sea) {
	if (this.time == this.breed && this.energy > 0) {
		this.time = 0;
		var baby = new Animal(this.type, this.x, this.y, this.color, this.breed, this.starve, this.changecolor);
		baby.add(sea);
	}
	return this;
}

// Bewegt ein Animal-Objekt auf ein benachbartes Feld
Animal.prototype.move = function (sea, dir) {
	if (dir == "N") {
		this.y = (this.y - 1 + sea[this.x].length) % sea[this.x].length;
	}
	if (dir == "O") {
		this.x = (this.x + 1 + sea.length) % sea.length;
	}
	if (dir == "S") {
		this.y = (this.y + 1 + sea[this.x].length) % sea[this.x].length;
	}
	if (dir == "W") {
		this.x = (this.x - 1 + sea.length) % sea.length;
	}
	return this;
}

// Gibt ein bestimmtes benachbartes Animal-Objekt eines Animal-Objekt zurück
Animal.prototype.get = function (sea, dir) {
	if (dir == "X") {
		return this;
	}
	if (dir == "N") {
		if (sea[this.x][(this.y - 1 + sea[this.x].length) % sea[this.x].length]) {
			return sea[this.x][(this.y - 1 + sea[this.x].length) % sea[this.x].length];
		}
	}
	if (dir == "O") {
		if (sea[(this.x + 1 + sea.length) % sea.length][this.y]) {
			return sea[(this.x + 1 + sea.length) % sea.length][this.y];
		}
	}
	if (dir == "S") {
		if (sea[this.x][(this.y + 1 + sea[this.x].length) % sea[this.x].length]) {
			return sea[this.x][(this.y + 1 + sea[this.x].length) % sea[this.x].length];
		}
	}
	if (dir == "W") {
		if (sea[(this.x - 1 + sea.length) % sea.length][this.y]) {
			return sea[(this.x - 1 + sea.length) % sea.length][this.y];
		}
	}
	return new Animal("empty", 0, 0, "", 0, 0);
}

// Zeichnet das Sea-Feld auf dem Canvas
function drawSea(sea) {
	var canvas = document.getElementById("sea");
	var ctx = canvas.getContext("2d");
	ctx.clearRect(0, 0, canvas.width, canvas.height);

	for (var i = 0; i < canvas.width; i++) {
		for (var j = 0; j < canvas.height; j++) {
			if (sea[i][j]) {
				sea[i][j].draw(ctx);
			}
		}
	}
}

// Speichert ein Bild des Canvas 
function saveSea() {
	var canvas = document.getElementById("sea");
	var data = canvas.toDataURL('image/jpeg');
	this.href = data;
	document.getElementById("downloadlink").download = "Wator_" + round + ".jpg";
}
document.getElementById("downloadlink").addEventListener("click", saveSea, false);

// Generiert ein neues Sea-Feld
function createSea(fish_count, fish_breed, fish_starve, fish_color, fish_changecolor, shark_count, shark_breed, shark_starve, shark_color, shark_changecolor) {
	var canvas = document.getElementById("sea");

	var sea = new Array(canvas.width);
	for (var i = 0; i < canvas.width; i++) {
		sea[i] = new Array(canvas.height);
	}

	for (var i = 0; i < fish_count; i++) {
		var fish = new Animal("fish", Math.floor(Math.random() * canvas.width), Math.floor(Math.random() * canvas.height), fish_color, fish_breed, fish_starve, fish_changecolor);
		fish.add(sea);
	}
	for (var i = 0; i < shark_count; i++) {
		var shark = new Animal("shark", Math.floor(Math.random() * canvas.width), Math.floor(Math.random() * canvas.height), shark_color, shark_breed, shark_starve, shark_changecolor);
		shark.add(sea);
	}
	return sea;
}

// Erzeugt eine Kopie eines Arrays
function copy(array) {
	return array.map(function (arr) {
		return arr.slice();
	});
}

// Entfernt einen Fisch und gibt dem Hai dessen Energie
function eat(_canvas, sea, shark, dir) {
	if (shark.get(sea, dir).type == "fish") {
		shark.energy = shark.energy + shark.get(sea, dir).energy;
		shark.get(sea, dir).remove(sea);
	}
	return shark;
}

// Erzeugt ein Array mit zufälligen einzigartigen Einträgen im Bereich von 0 bis length
function randomNumbers(length) {
	var array = new Array(length);
	for (var i = 0; i < array.length; i++) {
		array[i] = i;
	}
	for (var i = array.length - 1; i > 0; i--) {
		var j = Math.floor(Math.random() * (i + 1));
		var temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
	return array;
}

// Bewegt alle Animal-Objekte im Sea-Feld und ruft sich danach wieder auf
function moveAnimals(sea, time) {
	var canvas = document.getElementById("sea");
	var popul = 0,
		fishs = 0,
		sharks = 0;
	var sea_old = copy(sea);

	var arw = randomNumbers(canvas.width);

	for (var p = 0; p < canvas.width; p++) {
		var m = arw[p];
		var arh = randomNumbers(canvas.height);
		for (var q = 0; q < canvas.height; q++) {
			var n = arh[q];

			if (sea_old[m][n]) {
				popul++;
				var animal = sea_old[m][n];
				animal.changeColor();

				if (animal.type == "fish") {
					fishs++;
					var fish = animal;
					fish.remove(sea);

					// Alter erhöhen
					fish.time = (fish.time) % fish.breed + 1;

					// auf zufälliges freies Feld bewegen
					var num = Math.floor(Math.random() * 4);
					for (var j = 0; j < 4; j++) {
						if ((num + j) % 4 == 0 && fish.get(sea, "N").type == "empty") {
							fish = fish.reproduce(sea);
							fish = fish.move(sea, "N");
							break;
						}
						if ((num + j) % 4 == 1 && fish.get(sea, "O").type == "empty") {
							fish = fish.reproduce(sea);
							fish = fish.move(sea, "O");
							break;
						}
						if ((num + j) % 4 == 2 && fish.get(sea, "S").type == "empty") {
							fish = fish.reproduce(sea);
							fish = fish.move(sea, "S");
							break;
						}
						if ((num + j) % 4 == 3 && fish.get(sea, "W").type == "empty") {
							fish = fish.reproduce(sea);
							fish = fish.move(sea, "W");
							break;
						}
					}

					// wieder hinzufügen					
					fish.add(sea);
				}

				if (animal.type == "shark") {
					sharks++;
					var shark = animal;
					shark.remove(sea);

					// Alter erhöhen
					shark.time = (shark.time) % shark.breed + 1;
					shark.energy--;

					// Fische fangen oder auf zufälliges freies Feld bewegen
					var num = Math.floor(Math.random() * 4);
					for (var j = 0; j < 8; j++) {
						if ((num + j) % 4 == 0 && (shark.get(sea, "N").type == "fish" || (j >= 4 && shark.get(sea, "N").type == "empty"))) {
							shark = eat(canvas, sea, shark, "N");
							shark = shark.reproduce(sea);
							shark = shark.move(sea, "N");
							break;
						}
						if ((num + j) % 4 == 1 && (shark.get(sea, "O").type == "fish" || (j >= 4 && shark.get(sea, "O").type == "empty"))) {
							shark = eat(canvas, sea, shark, "O");
							shark = shark.reproduce(sea);
							shark = shark.move(sea, "O");
							break;
						}
						if ((num + j) % 4 == 2 && (shark.get(sea, "S").type == "fish" || (j >= 4 && shark.get(sea, "S").type == "empty"))) {
							shark = eat(canvas, sea, shark, "S");
							shark = shark.reproduce(sea);
							shark = shark.move(sea, "S");
							break;
						}
						if ((num + j) % 4 == 3 && (shark.get(sea, "W").type == "fish" || (j >= 4 && shark.get(sea, "W").type == "empty"))) {
							shark = eat(canvas, sea, shark, "W");
							shark = shark.reproduce(sea);
							shark = shark.move(sea, "W");
							break;
						}
					}

					// wieder hinzufügen und zeichnen falls noch Energie					
					if (shark.energy >= 0) {
						shark.add(sea);
					}
				}
			}
		}
	}
	drawSea(sea);
	round++;

	// if (round >= 0 && round <= 330) {
	// 	document.getElementById("downloadlink").click();
	// }
	console.log("Runde: " + round + " - Space: " + (canvas.width * canvas.height - popul) + " - Population: " + popul + " - Fische: " + fishs + " - Haie: " + sharks);

	setTimeout(function () {
		moveAnimals(sea, time);
	}, time);
}

// Erzeugt eine neue Wator-Instanz
function initialize() {
	var canvas = document.getElementById("sea");
	var formdata = sessionStorage.getItem("formdata");

	if (formdata !== null) {
		var values = {};
		$.each(JSON.parse(formdata), function (_i, field) {
			values[field.name] = field.value;
		});

		$("[name=fish_count]").val(values["fish_count"]);
		$("[name=fish_breed]").val(values["fish_breed"]);
		$("[name=fish_starve]").val(values["fish_starve"]);
		$("[name=fish_color]").val(values["fish_color"]);
		$("[name=fish_changecolor]").val(values["fish_changecolor"]);
		$("[name=shark_count]").val(values["shark_count"]);
		$("[name=shark_breed]").val(values["shark_breed"]);
		$("[name=shark_starve]").val(values["shark_starve"]);
		$("[name=shark_color]").val(values["shark_color"]);
		$("[name=shark_changecolor]").val(values["shark_changecolor"]);
		$("[name=world_width]").val(values["world_width"]);
		$("[name=world_height]").val(values["world_height"]);
		$("[name=world_time]").val(values["world_time"]);
		$("[name=world_blur]").val(values["world_blur"]);
		$("[name=world_contrast]").val(values["world_contrast"]);
		$("[name=world_saturate]").val(values["world_saturate"]);

		canvas.width = values["world_width"];
		canvas.height = values["world_height"];

		var sea = createSea(values["fish_count"], values["fish_breed"], values["fish_starve"], values["fish_color"], values["fish_changecolor"], values["shark_count"], values["shark_breed"], values["shark_starve"], values["shark_color"], values["shark_changecolor"]);
		moveAnimals(sea, values["world_time"]);

	} else {
		$("[name=fish_count]").val(1000);
		$("[name=fish_breed]").val(1);
		$("[name=fish_starve]").val(1);
		$("[name=fish_color]").val("#00FF00");
		$("[name=fish_changecolor]").val(0);
		$("[name=shark_count]").val(200);
		$("[name=shark_breed]").val(1);
		$("[name=shark_starve]").val(1);
		$("[name=shark_color]").val("#0000FF");
		$("[name=shark_changecolor]").val(0);
		$("[name=world_width]").val(Math.floor($(window).width() / 10));
		$("[name=world_height]").val(Math.floor($(window).height() / 10));
		$("[name=world_time]").val(30);
		$("[name=world_blur]").val(10);
		$("[name=world_contrast]").val(1100);
		$("[name=world_saturate]").val(200);

		canvas.width = $(window).width() / 10;
		canvas.height = $(window).height() / 10;

		var sea = createSea(1000, 1, 1, "#00FF00", 0, 200, 1, 1, "#0000FF", 0);
		moveAnimals(sea, 30);

		setTimeout(function () {
			$("#anleitung").modal("show");
		}, 3000);
	}
}

// Lade Colorpicker
$('input[name="fish_color"]').colpick({
	submit: false,
	onChange: function (_hsb, hex, _rgb, _el, _bySetColor) {
		$('input[name="fish_color"]').val('#' + hex.toUpperCase());
	}
});
$('input[name="shark_color"]').colpick({
	submit: false,
	onChange: function (_hsb, hex, _rgb, _el, _bySetColor) {
		$('input[name="shark_color"]').val('#' + hex.toUpperCase());
	}
});

initialize();

// Wendet den Blur-Filter an
$(document).ready(function () {
	var formdata = sessionStorage.getItem("formdata");
	if (formdata !== null) {
		$("#sea").css("filter", "blur(" + JSON.parse(formdata)[11]["value"] + "px) contrast(" + JSON.parse(formdata)[12]["value"] + "%) saturate(" + JSON.parse(formdata)[13]["value"] + "%)");
	} else {
		$("#sea").css("filter", "blur(10px) contrast(1100%) saturate(200%)");
	}
});

// Öffnet die Anleitung
$("#sea").click(function () {
	$("#anleitung").modal("show");
});

// Speichert die übergebenen Startdaten bevor das Fenster neu aufgerufen wird
$("#form").submit(function () {
	sessionStorage.setItem("formdata", JSON.stringify($("#form").serializeArray()));
});